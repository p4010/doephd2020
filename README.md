# DoEPhD2020
Course on Design and Analysis of Experiments for PhD students, year 2020.

# Prepare for the course

## On Mac and Linux
You need a working LaTeX installation and then you have to:
* [Download](http://cran.mirror.garr.it/mirrors/CRAN/) and install R-Project
* [Download](https://rstudio.com/) and install RStudio

## On Windows
* [Download](http://cran.mirror.garr.it/mirrors/CRAN/) and install R-Project
* [Download](https://rstudio.com/) and install RStudio
* In RStudio, create a new notebook, save it, select "Preview Notebook", and wait for it installing needed components (it takes some time)
* [Download](https://miktex.org/download) and install MiXTeX
* Close and reopen RStudio
* Open the same test notebook, and select "Knit to PDF". It is probably going to fail with an error.
* In RStudio, select the Files panel, navigate to the folder containing the test.Rmd file and select "Set as working directory" in the "More" drop down menu (the one with the blue gear)
* Open a new terminal (Menu Tools>Terminal>New Terminal), and in the terminal type `pdflatex test.tex`: some windows will pop up asking for installing components, accept and wait. If the terminal command ends with a question markl, type enter and wait for the command to complete.
* Try again to knit the pdf, this time it should succeed and present yoy the formatted pdf file.